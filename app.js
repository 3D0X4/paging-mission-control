/*Full disclosure.  I had to rewrite this code to avoid any chance of plagiarism. 
 About halfway through the project I ended up finding the answer online, and it was eerily close to my original work.
 The below is the code from online.  Found @ https://stackoverflow.com/questions/63329955/comparing-objects-within-the-same-array-javascript
const res = arr.reduce((acc, {timestamp,satelliteid:satelliteId,redlow,redhigh,component})=>{
  const getDate = str => new Date(str.slice(0,4)+'-'+str.slice(4,6)+'-'+str.slice(6));
  const date = getDate(timestamp);
  let severity;
  if(arr.filter(obj=>+obj.rawvalue < +redlow && getDate(obj.timestamp) - date <= 5 * 60 * 1000).length >= 3){
    severity = 'RED LOW';
  } else if(arr.filter(obj=>+obj.rawvalue > +redhigh && getDate(obj.timestamp) - date <= 5 * 60 * 1000).length >= 3){
    severity = 'RED HIGH';
  }
  if(severity){
    acc.push({satelliteId,severity,component,timestamp});
  }
  return acc;
}, []);
console.log(res);
*/



//adding event listener 
function init(){
    document.getElementById('fileInput').addEventListener('change', handleFileSelect, false);
  }
  //reading the inputted file
  function handleFileSelect(event){
    const reader = new FileReader()
    reader.onload = handleFileLoad;
    reader.readAsText(event.target.files[0])
  }

function handleFileLoad(event){
  var dataString = event.target.result;
  //splitting array based on newline
  var dataArr = dataString.split(/\r?\n/);
  //splitting dataArray based on each |
  let newArr = dataArr.map((string) => {
    return string.split("|");
  });
  //building new object 
  let satData = newArr.map(obj => ({
    timeStamp: obj[0],
    key: obj[1],
    red_High_Limit: obj[2],
    yellow_High_Limit: obj[3],
    yellow_Low_Limi: obj[4],
    red_Low_Limit: obj[5],
    raw_Value: obj[6],
    component: obj[7],
    severity: "severity"
  }));
  getAlerts(satData)
}

function getAlerts(satellites) {
  //create array that will get the high alerts and low alerts
  let satAlertsHigh = [];
  let satAlertsLow = [];
//looping 
    const redAlertLowSats = satellites.filter(element => {
      const highLimit = Number(element.red_High_Limit)//changing values to numbers so i can evaulaute them later
      const lowLimit = Number(element.red_Low_Limit)
      const value = Number(element.raw_Value)   

      if (highLimit < value) { //checking if the highlimit is greater than the current value of the satterliate
        satAlertsHigh.push(element)//pushing onto an array the values that are to high
      }
      else if (lowLimit > value ) {
        satAlertsLow.push(element)//pushing onto an array the values that are to low
      }
    })
    //checking if the alerts occured within 5 minutes of each other
    let alertsThatWereWithinFiveMinutesAndHigh = satAlertsHigh.filter(e => {
      if (getTheRightDate(satAlertsHigh[0].timeStamp) - getTheRightDate(satAlertsHigh[satAlertsHigh.length -1].timeStamp) < 30000) {
        return e;
      }
    })
      //checking if the alerts occured within 5 minutes of each other
    let alertsThatWereWithinFiveMinutesAndLow = satAlertsLow.filter(e => {
      if (getTheRightDate(satAlertsLow[0].timeStamp) - getTheRightDate(satAlertsLow[satAlertsLow.length -1].timeStamp) < 30000) {
        return e;
      }
    })
    //calling the funnction to log them
    displayFinalToConsole(alertsThatWereWithinFiveMinutesAndHigh)
    displayFinalToConsole(alertsThatWereWithinFiveMinutesAndLow)
  }
  //parsing date to get a usable format
  function getTheRightDate(timeStamp) {
    return date = new Date(timeStamp.slice(0,4)+'-'+timeStamp.slice(4,6)+'-'+timeStamp.slice(6));
  }
//final display
  function displayFinalToConsole(items) {
    let groups = {}; 
    let finalObj = [];           // An empty object to store the groups in
    for (let object of items) {   // Loop over all your objects
      if (!groups[object.key]) {   // If we don't have a group for this key
        groups[object.key] = [];   // create a new array and add it to the groups object
      }
      groups[object.key].push(object); // Add the object to the group (array) identified by the key.
    }

    for (let obj of Object.values(groups)) { //final logging of sats that have at least 3
      if (obj.length > 2) {
        console.log(obj)
      }
    }
}
